//
//  ViewController.m
//  JSONCreator
//
//  Created by Ankur on 16/01/15.
//  Copyright (c) 2015 your company. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
{
    NSMutableString *curElem;
    NSMutableDictionary *dictMut;

}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    dictMut=[[NSMutableDictionary alloc]init];
    // Do any additional setup after loading the view, typically from a nib.
    
    [self sampleData2Plist];
    [self sampleDataPlist];
    
//    NSURL *url=[[NSURL alloc] initWithString:WSDL_ActivityLockService]; // Write your file path here
//    NSXMLParser *XML=[[NSXMLParser alloc] initWithContentsOfURL:url];
//    XML.delegate=self;
//    [XML parse];
    

    [self CreateJsonFromNString];
    
}


-(void)CreateJsonFromNString{
    
    NSString *myRequestString = @"https://m.youtube.com/watchU+003Fv=obU9tH5msOM";
    NSString *requestStr = [myRequestString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    NSString *requestString=[NSString stringWithFormat:@"{\"request\":{\"type\":\"post_data\",\"devicetype\":\"I\"},\"userinfo\":{\"userid\":\"_48\",\"sessionid\":\"af09f9ac3d02898813ede8ba141a5628\"},\"requestinfo\":{\"post_type\":\"V\",\"post_id\":\"\",\"title\":\"jjjjkkkkk\",\"category\":\"2\",\"location\":\" Noida , Uttar Pradesh\",\"latitude\":\"28.612679\",\"longitude\":\"77.383835\",\"post_data\":\"%@\"}}",requestStr];
    NSDictionary *jsonObject = [NSJSONSerialization JSONObjectWithData:[requestString dataUsingEncoding:NSUTF8StringEncoding]
                                                          options:0 error:NULL];
    NSLog(@"jsonObject %@",jsonObject);
    
    
}

-(void)EncodingDecoding{

    NSString *url = @"https://m.youtube.com/watch?v=0vbasldkur&a=84";
    NSString *apiKey = (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(NULL, (CFStringRef)url, NULL, (CFStringRef)@"!*'();:@&=+$,/?%#[]", kCFStringEncodingUTF8));
    
    NSString *reverseKey = (NSString*)CFBridgingRelease(CFURLCreateStringByReplacingPercentEscapesUsingEncoding(NULL, (CFStringRef)apiKey, CFSTR(""),kCFStringEncodingUTF8));

  NSLog(@"url =%@",apiKey);
   NSLog(@"reversekey %@",reverseKey);
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
    
}

-(void)sampleData2Plist{
    
    //to get plist path
    NSString* plistPath = [[NSBundle mainBundle] pathForResource:@"SampleData2" ofType:@"plist"];
    // getting complete plist dict
    NSDictionary * fullcontentDict = [NSDictionary dictionaryWithContentsOfFile:plistPath];
    
    // taking out service array
    NSArray *serviceArrayMain=[fullcontentDict objectForKey:@"Service"];
    NSLog(@"servicearray %@",serviceArrayMain);
    
    NSMutableArray *serviceArray=[[NSMutableArray alloc]init];
    
    // creating array for service
    for (int i=0; i<[serviceArrayMain count]; i++) {
        NSDictionary *dictService=[serviceArrayMain objectAtIndex:i];
        NSString *serviceNameStr=[dictService objectForKey:@"name"];
        [serviceArray addObject:serviceNameStr];
    }
    NSLog(@"service array %@",serviceArray);
    
 
    // after selecting first value from picker that is hair service
    NSString *pickerFirstValue=[serviceArray objectAtIndex:0];
    // to get picker ID
    NSString *pickerStringId;
    for (int i=0; i<[serviceArrayMain count]; i++) {
        NSDictionary *dict=[serviceArrayMain objectAtIndex:i];
        if ([[dict objectForKey:@"name"]isEqualToString:pickerFirstValue]) {
            pickerStringId=[dict objectForKey:@"id"];
        }
    }
    NSLog(@"final pickerStringId %@",pickerStringId);
    
    
    // to get the service of hair
    NSDictionary *SubServiceDict=[serviceArrayMain objectAtIndex:0];
    NSArray *SubServiceArray=[SubServiceDict objectForKey:@"subservice"];

    NSMutableArray *subServiceArrayNames=[[NSMutableArray alloc]init];
    // creating array for sub service
    for (int i=0; i<[SubServiceArray count]; i++) {
        NSDictionary *dictService=[SubServiceArray objectAtIndex:i];
        NSString *serviceNameStr=[dictService objectForKey:@"sub_name"];
        [subServiceArrayNames addObject:serviceNameStr];
    }
    NSLog(@"subServiceArrayNames %@",subServiceArrayNames);

    
    // after selcting subpicker 2 value get its id
    NSString *pickerString=[subServiceArrayNames objectAtIndex:1];
    NSString *pickerSubStringId;
    for (int i=0; i<[SubServiceArray count]; i++) {
        NSDictionary *dict=[SubServiceArray objectAtIndex:i];
        if ([[dict objectForKey:@"sub_name"]isEqualToString:pickerString]) {
            pickerSubStringId=[dict objectForKey:@"sub_id"];
        }
    }
    
    NSLog(@"final pickerSubStringId %@",pickerSubStringId);
}


-(void)sampleDataPlist{

    //to get plist path
    NSString* plistPath = [[NSBundle mainBundle] pathForResource:@"SampleData" ofType:@"plist"];
    // getting complete plist dict
    NSDictionary * fullcontentDict = [NSDictionary dictionaryWithContentsOfFile:plistPath];
    
    // taking out service dict
    NSDictionary *serviceDict=[fullcontentDict objectForKey:@"Service"];
    NSLog(@"serviceDict %@",serviceDict);
    
    // taking out all keys of service dict
    NSArray *arrayServiceName=[serviceDict allKeys];
    NSLog(@"arrayServiceName %@",arrayServiceName);
    
    //if selected picker value is first
    NSString *anySelctedServicePickerValue =  [arrayServiceName objectAtIndex:0];
    NSLog(@"anySelctedPickerValue %@",anySelctedServicePickerValue);
    // getting dictionary of first selected picker value
    NSDictionary *ServiceDict2=[serviceDict objectForKey:anySelctedServicePickerValue];
    
    //to get service ID
    NSString *anySelctedServicePickerValueID=[ServiceDict2 objectForKey:@"ID"];;
    NSLog(@"anySelctedPickerValueID %@",anySelctedServicePickerValueID);
    
    // to get their subservices
    NSDictionary *subServiceDict=[ServiceDict2 objectForKey:@"Subservices"];
    // getting all keys of Subservices to fill in array
    NSArray *categoryArrayService=[subServiceDict allKeys];
    NSLog(@"category %@",categoryArrayService);
    
    NSString *anySelctedSubPickerValue =  [categoryArrayService objectAtIndex:0];
    NSLog(@"anySelctedSubPickerValue %@",anySelctedSubPickerValue);
    
    // to get their subserviceID
    
    NSDictionary *subServiceCategoryDict=[subServiceDict objectForKey:anySelctedSubPickerValue];
    NSString *anySelctedSubServicePickerValueID=[subServiceCategoryDict objectForKey:@"ID"];;
    NSLog(@"anySelctedSubPickerValueID %@",anySelctedSubServicePickerValueID);
    
    
    
    
    // to get Json String
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:serviceDict
                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                         error:&error];
    
    if (! jsonData) {
        NSLog(@"Got an error: %@", error);
    } else {
        NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        NSLog(@"serviceDictjsonString %@",jsonString);
        
    }
    

}

-(void) parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict
{
    
    NSLog(@"Processing Element: %@", elementName);
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string {
    
    if(!curElem)
        curElem = [[NSMutableString alloc] initWithString:string];
    else
        [curElem appendString:string];
    
  //  NSLog(@"Processing Value: %@", curElem);
    
}


- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName
  namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    NSLog(@"elementName: %@", elementName);

   /*
    if([elementName isEqualToString:@"Books"])
        return;
    
    if([elementName isEqualToString:@"Book"]) {
        [appdelegate.books addObject:book];
        
        [book release];
        book = nil;
    }
    else
        [book setValue:curElem forKey:elementName];
    
    [curElem release];
    curElem = nil;*/
    
    [dictMut setValue:curElem forKey:elementName];

    
    
}





@end
